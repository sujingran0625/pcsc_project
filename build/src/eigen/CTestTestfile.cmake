# CMake generated Testfile for 
# Source directory: /Users/suo/Desktop/projectpcsc/pcsc_project/src/eigen
# Build directory: /Users/suo/Desktop/projectpcsc/pcsc_project/build/src/eigen
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("doc")
subdirs("test")
subdirs("failtest")
subdirs("blas")
subdirs("lapack")
subdirs("unsupported")
subdirs("demos")
subdirs("scripts")
subdirs("bench/spbench")
