#pragma once
#ifndef __TEST__H__
#define __TEST__H__
#include <iostream>
#include <Eigen/Dense>
using namespace std;

float f_vector(vector<float> v, vector<float> v_solve);

float chord_test(string path);

float newton_test(string path);

float bisection_test(string path);

float fixed_point_test(string path);

float aitken_test(string path);

float f_equations(Eigen::MatrixXf m, Eigen::VectorXf vec);

float equations_test(string path);

#endif
