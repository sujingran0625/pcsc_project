#include <iostream>
#include <vector>
#include "function.h"
#include "implement.h"
#include "math.h"
#include "test.h"
#include <stdlib.h>  

using namespace std;

float f_vector(vector<float> v, vector<float> v_solve) {

	int size = v.size();
	int type = v[size - 1]; // get the number of the last vector
	v.pop_back();
	int n = v.size();
	int m = v_solve.size();
	float a = 0;
	for (int i = 0; i < m; i++)
	{
		float x = v_solve[i];
		if (type == 1)
		{
			for (int i = 0; i < n; i++)
			{
				a += v[i] * pow(x, n - i - 1);
			}
		}
		else if (type == 2)
		{
			a = v[0] * sin(v[1] * x + v[2]) + v[3] * cos(v[4] * x + v[5]) + v[6];
		}
		else
		{
			for (int i = 0; i < (n - 1) / 2; i++)
			{
				a += v[2 * i] * exp(v[2 * i + 1] * x);
			}
			a += v[n - 1];
		}
	}
	a = a / m;

	return a;

}

float chord_test(string path)
{
	float diff = 0;
	vector<float> v_read = Read_File_txt(path);
	vector<float> v_out;
	Imple_chord r1(v_read);
	r1.func();
	v_out = r1.getpoint();
	int n = v_out.size();
	diff = f_vector(v_read, v_out);
	return diff;
}

float newton_test(string path)
{
	float diff = 0;
	vector<float> v_read = Read_File_txt(path);
	vector<float> v_out;
	Imple_newton r1(v_read);
	r1.func();
	v_out = r1.getpoint();
	int n = v_out.size();
	diff = f_vector(v_read, v_out);
	return diff;
}

float bisection_test(string path)
{
	float diff = 0;
	vector<float> v_read = Read_File_txt(path);
	vector<float> v_out;
	Imple_bisection r1(v_read);
	r1.func();
	v_out = r1.getpoint();
	int n = v_out.size();
	diff = f_vector(v_read, v_out);
	return diff;
}

float fixed_point_test(string path)
{
	float diff = 0;
	vector<float> v_read = Read_File_txt(path);
	vector<float> v_out;
	Imple_fixed_point r1(v_read);
	r1.func();
	v_out = r1.getpoint();
	int n = v_out.size();
	diff = f_vector(v_read, v_out);
	return diff;
}

float aitken_test(string path)
{
	float diff = 0;
	vector<float> v_read = Read_File_txt(path);
	vector<float> v_out;
	Imple_fixed_point_aitken r1(v_read);
	r1.func();
	v_out = r1.getpoint();
	int n = v_out.size();
	diff = f_vector(v_read, v_out);
	return diff;
}

float f_equations(Eigen::MatrixXf m, Eigen::VectorXf vec)
{
	// v is the solution of x1,x2,...,xn
	int row = m.rows();
	float a = 0;
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < row; j++)
		{
			a += m(i, 3 * j) * pow(vec[j], 3) + m(i, 3 * j + 1) * pow(vec[j], 2) + m(i, 3 * j + 2) * vec[j]; // 3a*x^3 + 2b*x^2 + c*x
		}
		a += m(i, 3 * row);
	}
	return a;
}

float equations_test(string path)
{
	float diff = 0;
	Eigen::VectorXf v;
	Eigen::MatrixXf m_read = Read_matrix_txt(path);
	Equations r1(m_read); 
	r1.func();
	r1.get_output();
	Eigen::VectorXf v_solve = r1.getpoint();
	diff = f_equations(m_read, v_solve);
	return diff;
}